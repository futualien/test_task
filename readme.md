## TEST TASK FOR DEVELOPER

** I used laravel4 because it don't want install php7 right now.**

## Requirements

php5.3, composer, phpunit

## INSTALLATION

Do git clone in your own directory where you want.

Create MySQL database with user and write config "PROJECT_DIR/app/config/database.php"

Run command:

**cd PROJECT_DIR && composer update && php artisan migrate:install && php artisan db:seed**

for run test: **cd PROJECT_DIR && phpunit**

after that execute: **php artisan serve**

Now the api is ready for working.

The data format is json for modify is 

for groups:

*{'group': {'name': TEXT:max50, 'users': [userEntity, ...]}*

for users:

*{'user': {'email': TEXT:email, 'first_name': TEXT:max50, 'last_name': TEXT:max50, 'groups': [groupEntity, ...]}}*

if field users/groups doesnt exists then it not changed.

list of entities format:

*{'groups': [{'id', 'name'}, ... ]}*

*{'users': [{'id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at', 'groups': [groupEntity]}, ...]}*