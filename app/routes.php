<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('main');
});

/* ~=< THERE IS API, SO MANY CODE!  >=~ */

Route::group(['prefix' => 'api'], function(){
	Route::resource('users', 'App\Api\Controllers\UserController');
	Route::resource('groups', 'App\Api\Controllers\GroupController');
});


