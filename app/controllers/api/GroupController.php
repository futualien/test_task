<?php
namespace App\Api\Controllers;
use App\Models\Group;
use App\Models\User;
use App\Validators\GroupValidator;
use Response;
use Request;

class GroupController extends ApiController {
		
	/**
	* Fetch list of the items.
	*
	* @return Response
	*/
	public function index() {
		$groups = Group::all();
		return Response::json($groups->toArray());
	}
	
	/**
	* Update the specified item in database.
	*
	* @param  int $id
	* @return Response
	*/
	public function update($id) {
		$data = Request::all()['group'];
        $validate = GroupValidator::validated($data);
        if(!$validate->fails()) {
            $group = Group::find($id)->fill($data);
			$group->save();
			if (isset($data['users'])) {
				$userService = \App::make('UserService');
				//clear previous groups
				$userService->clearGroup($group);
				
				foreach($data['users'] as $user) {
					$userService->addUserInGroup(User::find($user['id']), $group);
				}
			}
            return Response::json([
                'group' => $group->toArray()
            ], 200);
        } else {
            return Response::json([
                'errors' => $validate->messages()
            ], 422);
        }
	}
	
	/**
	* Store a newly created item in database.
	*
	* @return Response
	*/
	public function store() {
		$data = Request::all()['group'];
        $validate = GroupValidator::validated($data);
        if(!$validate->fails()) {
            $group = Group::create($data);
			if (isset($data['users'])) {
				$userService = \App::make('UserService');
				
				foreach($data['users'] as $user) {
					$userService->addUserInGroup(User::find($user['id']), $group);
				}
			}
            return Response::json([
                'group' => $group->toArray()
            ], 200);
        } else {
            return Response::json([
                'errors' => $validate->messages()
            ], 422);
        }
	}
	
	/**
	* Display the specified item.
	*
	* @param  int $id
	* @return Response
	*/
	public function show($id) {
		$group = Group::find($id);
		return Response::json(['group' => $group->toArray()]);
	}
	
	
	/**
	* Remove the specified item from database.
	*
	* @param  int $id
	* @return Response
	*/
	public function destroy($id) {
		return Response::json(['error' => !Group::find($id)->delete()]);
	}
	
}
