<?php
namespace App\Api\Controllers;

use App;
use Controller;
use Response;

/**
 * Контроллер для RESTfull API
 * Class ApiController
 * @package Common\Controllers
 */
class ApiController extends Controller
{

    public function __construct()
    {

        /**
         * Все 404 будем отдавать в json
         */
        App::missing(function ($exception) {
            return Response::json(["status" => "error", "code" => 404, "message" => $exception->getMessage()]);
        });
    }
}