<?php
namespace App\Api\Controllers;
use App\Models\User;
use App\Models\Group;
use App\Models\UserGroups;
use App\Validators\UserValidator;
use Response;
use Request;

class UserController extends ApiController {
		
	/**
	* Fetch list of the items.
	*
	* @return Response
	*/
	public function index() {
		$users = User::all();		
		return Response::json($users->toArray());
	}
	
	/**
	* Update the specified item in database.
	*
	* @param  int $id
	* @return Response
	*/
	public function update($id) {
		$data = Request::all()['user'];
        $validate = UserValidator::validated($data);
        if(!$validate->fails()) {
			
            $user = User::find($id)->fill($data);
			$user->save();
			if (isset($data['groups'])) {
				$userService = \App::make('UserService');
				//clear previous groups
				$userService->removeUserFromGroups($user);
				
				foreach($data['groups'] as $group) {
					$userService->addUserInGroup($user, Group::find($group['id']));
				}
			}
			
            return Response::json([
                'user' => $user->toArray()
            ], 200);
        } else {
            return Response::json([
                'errors' => $validate->messages()
            ], 422);
        }
	}
	
	/**
	* Store a newly created item in database.
	*
	* @return Response
	*/
	public function store() {
		$data = Request::all()['user'];
        $validate = UserValidator::validated($data);
        if(!$validate->fails()) {
            $user = User::create($data);
			
			if (isset($data['groups'])) {
				$userService = \App::make('UserService');				
				foreach($data['groups'] as $group) {
					$userService->addUserInGroup($user, Group::find($group['id']));
				}
			}
			
            return Response::json([
                'user' => $user->toArray()
            ], 200);
        } else {
            return Response::json([
                'errors' => $validate->messages()
            ], 422);
        }
	}
	
	/**
	* Display the specified item.
	*
	* @param  int $id
	* @return Response
	*/
	public function show($id) {
		$user = User::find($id);
		return Response::json(['user' => $user->toArray()]);
	}
	
	
	/**
	* Remove the specified item from database.
	*
	* @param  int $id
	* @return Response
	*/
	public function destroy($id) {
		$user = User::find($id);
		//this is bad, but event in User model dont work I dont know why
		UserGroups::where('user_id', $user->id)->delete();
		//====
		return Response::json(['error' => !$user->delete()]);
	}
	
}
