<?php
use App\Models\User;
use App\Models\Group;

class ApiTest extends TestCase {

	private function getRandomUser(){
		$users = User::all();
		return $users[rand(0, $users->count()-1)];
	}

	private function getRandomGroup(){
		$groups = Group::all();
		return $groups[rand(0, $groups->count()-1)];
	}
	
	private function getRandomGroups() {
		$groups = Group::all();
		$groupsCount = $groups->count();
		$selectedGroups = [];
		//select few random groups
		for ($i=0; $i<$groupsCount; $i++) {
			if (rand(0,1)) {
				$selectedGroups[] = $groups[$i]->toArray();
			}
		}
		
		return $selectedGroups;
	}
	
	private function getRandomUsers() {
		$users = User::all();
		$usersCount = $users->count();
		$selectedUsers = [];
		//select few random users
		for ($i=0; $i<$usersCount; $i++) {
			if (rand(0,1)) {
				$selectedUsers[] = $users[$i]->toArray();
			}
		}
		
		return $selectedUsers;
	}

	/**
	 * A api functional tests.
	 *
	 * @return void
	 */
	public function testUserListApi()
	{
		$response = $this->call('GET', '/api/users');
		$json = json_decode($response->getContent(), true);
		
		//api is answering
		$this->assertResponseOk();
		
		//count of table equals response
		$this->assertTrue(count($json) == User::all()->count());
		
		//the count of groups is equals for random user
		$key = rand(0, (count($json)-1)); //I dont know how many users exists exactly
		$id = $json[$key]['id'];
		$user = User::find($id);
		$groups = $user->groups()->get();
		//groups and data recieved through api must be equal
		$this->assertEquals($json[$key], $user->toArray());
		$this->assertEquals($json[$key]['groups'], $groups->toArray());
	}
	
	public function testFailCreateUser() {
		
		$response = $this->call('POST', '/api/users', ['user'=>['email'=>'failmail', 'first_name'=>'Test fail', 'last_name'=>'fail test', 'groups'=>['id'=>1, 'name'=>'test']]]);
		$json = json_decode($response->getContent(), true);
		
		//should be error in email
		$this->assertArrayHasKey('email', $json['errors']);
		
	}
	
	
	public function testCreateAndDeleteUser() {
		
		$selectedGroups = $this->getRandomGroups();
		
		$response = $this->call('POST', '/api/users', ['user'=>['email'=>'success@email.ru', 'first_name'=>'Test success', 'last_name'=>'Success test', 'groups'=>$selectedGroups]]);
		$json = json_decode($response->getContent(), true);
		
		//created user id
		$id = $json['user']['id'];
		
		//check is this user created
		$user = User::find($id);
		$this->assertNotEmpty($user);
		
		//check is user in requested groups
		$this->assertEquals($selectedGroups, $user->groups()->get()->toArray());
		
		
		$response = $this->call('DELETE', '/api/users/'.$id);
		$json = json_decode($response->getContent(), true);
		//the user was removed (check answer)
		$this->assertFalse($json['error']);
		
		$users = User::all();
		$idList = [];
		foreach($users as $user) {
			$idList[] = $user->id;
		}
		//there is no user with $id physycaly
		$this->assertArrayNotHasKey($id, $idList);
	}
	
	public function testFailModifyUser() {
		$user = $this->getRandomUser();
		
		$response = $this->call('PUT', '/api/users/'.$user->id, ['user'=>['email'=>'newwrong', 'first_name'=>'Update fail', 'last_name'=>'Failed update', 'groups'=>[]]]);
		$json = json_decode($response->getContent(), true);
		//should be error in email
		$this->assertArrayHasKey('email', $json['errors']);
		
		//check is any changes in user data ?
		$userData = User::find($user->id);
		
		$this->assertEquals($userData->toArray(), $user->toArray());
		
		$response = $this->call('PUT', '/api/users/'.$user->id, ['user'=>['email'=>'newwrong@mail.ru', 'first_name'=>'Update fail very long text in the first name field because of this shouldnt be working fine', 'last_name'=>'Failed update', 'groups'=>[]]]);
		$json = json_decode($response->getContent(), true);
		//should be error in email
		$this->assertArrayHasKey('first_name', $json['errors']);
		
		//check is any changes in user data ?
		$userData = User::find($user->id);
		
		$this->assertEquals($userData->toArray(), $user->toArray());
		
	}
	
	public function testModifyUser() {
		
		$user = $this->getRandomUser();
		$selectedGroups = $this->getRandomGroups();
		
		$response = $this->call('PUT', '/api/users/'.$user->id, ['user'=>['email'=>'newemail@gmail.com', 'first_name'=>'Update success', 'last_name'=>'Success update', 'groups'=>$selectedGroups]]);
		$json = json_decode($response->getContent(), true);
	 
		//check is changes in user data in database?
		$userData = User::find($user->id);
		$this->assertNotEquals($userData->toArray(), $user->toArray());
		
		//restore old data
		$response = $this->call('PUT', '/api/users/'.$user->id, ['user'=>$user->toArray()]);
		$json = json_decode($response->getContent(), true);
		//check is changes in user data in database?
		$userData = User::find($user->id);
		$this->assertEquals($userData->toArray()['email'], $user->toArray()['email']);
		$this->assertNotEquals($userData->toArray()['updated_at'], $user->toArray()['updated_at']);
		
	}
	
	public function testGetUserInfo() {
		$user = $this->getRandomUser();
		
		$response = $this->call('GET', '/api/users/'.$user->id);
		$json = json_decode($response->getContent(), true);
		
		//check is real data same as recieved from api
		$this->assertEquals($user->toArray(), $json['user']);
		
		//check data; is user active
		$this->assertEquals('active', $json['user']['state']);
		
	}
	
	
	public function testCreateAndDeleteGroup() {
		$response = $this->call('POST', '/api/groups', ['group'=>['name'=>'NEW GROUP TEST']]);
		$json = json_decode($response->getContent(), true);
		
		//created group id
		$id = $json['group']['id'];
		
		$group = Group::find($id);
		
		$this->assertEquals($group->toArray(), $json['group']);
		
		$response = $this->call('DELETE', '/api/groups/'.$group->id);
		$json = json_decode($response->getContent(), true);
		//the group was removed (check answer)
		$this->assertFalse($json['error']);
		
		$groups = Group::all();
		$idList = [];
		foreach($groups as $group) {
			$idList[] = $group->id;
		}
		//there is no group with $id physycaly
		$this->assertArrayNotHasKey($id, $idList);
		
	}
	
	
	public function testModifyGroup() {
		
		$group = $this->getRandomGroup(); //random group
		$users = $group->users()->get()->toArray();
		$selectedUsers = $this->getRandomUsers();
		
		$response = $this->call('PUT', '/api/groups/'.$group->id, ['group'=>['name' => 'new group name', 'users'=>$selectedUsers]]);
		$json = json_decode($response->getContent(), true);
	 
		//check is changes in group data in database?
		$groupData = Group::find($group->id);
		$this->assertNotEquals($groupData->toArray(), $group->toArray());
		
		//restore old data
		$groupArr = $group->toArray();
		$groupArr['users'] = $group->users()->get()->toArray();
		$response = $this->call('PUT', '/api/groups/'.$group->id, ['group'=>$groupArr]);
		$json = json_decode($response->getContent(), true);
		//check is changes in group data in database?
		$groupData = Group::find($group->id);
		$this->assertEquals($groupData->toArray(), $group->toArray());
		
		$this->assertEquals($groupData->users()->get()->toArray(), $group->users()->get()->toArray());
	}
	
	public function testGetGroupInfo() {
		$group = $this->getRandomGroup();
		
		$response = $this->call('GET', '/api/groups/'.$group->id);
		$json = json_decode($response->getContent(), true);
		
		//check is real data same as recieved from api
		$this->assertEquals($group->toArray(), $json['group']);
		
		
	}
	
}
