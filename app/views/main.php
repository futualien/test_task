<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
		.welcome .left, .welcome .center, .welcome .right {
			display: inline-block;
			width: 30%;
		}
		.welcome .left {
			float: left;
		}
		.welcome .right {
			float: right;
		}
		
	</style>
</head>
<body>
	<div class="welcome">
		<h1>User stories</h1>
		<div class="left">
			<h1>I want to create a user who is included in a group</h1>
			<div class="form">
				
			</div>
		</div>
		
		<div class="center">
			<h1>I want to check if this user exists and is active</h1>
			
		</div>
		
		<div class="right">
			<h1>I want to modify the list of users of a group</h1>
		</div>
	</div>
</body>
</html>
