<?php

use App\Services\UserService;

App::bind('UserService', function(){
    return new UserService();
});
