<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//пользователи
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('last_name');
			$table->string('first_name');			
			$table->boolean('state')->default(true);
			$table->timestamps();
		});
		
		//группы
		Schema::create('groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
		});
		
		//связь пользователей с группами
		Schema::create('user_to_groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('group_id');
		});
		
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('groups');
		Schema::drop('user_to_groups');
	}

}
