<?php
use App\Models\Group;
use App\Models\User;

class UserGroupSeeder extends Seeder
{
    public function run()
    {
        DB::table('user_to_groups')->delete();
        
		$users = User::all();
		$groups = Group::all();
		$userService = App::make('UserService');
		
		//user 0 have 2 groups
		$userService->addUserInGroup($users[0], $groups[0]);
		$userService->addUserInGroup($users[0], $groups[1]);
		
		//user 1 have 2 groups with collide
		$userService->addUserInGroup($users[1], $groups[0]);
		$userService->addUserInGroup($users[1], $groups[3]);
		
		//user 2 havent groups
		
		//user 3 have other group
		$userService->addUserInGroup($users[3], $groups[2]);
		
    }
} 