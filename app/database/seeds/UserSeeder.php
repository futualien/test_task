<?php
use App\Models\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        
		$data = [
				['email' => 'foo@bar.ru', 'last_name' => 'Foo', 'first_name' => 'Bar'],
				['email' => 'bar@foo.ru', 'last_name' => 'Bar', 'first_name' => 'Foo'],
				['email' => 'test@test.ru', 'last_name' => 'Test', 'first_name' => 'Test'],
				['email' => 'man@humans.ru', 'last_name' => 'Man', 'first_name' => 'Humans'],
		];
		
		foreach($data as $item) {
			User::create($item);	
		}
		
    }
} 