<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//common seeds
		$this->call('UserSeeder');
		$this->call('GroupSeeder');
		$this->call('UserGroupSeeder');
	}

}
