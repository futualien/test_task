<?php
use App\Models\Group;

class GroupSeeder extends Seeder
{
    public function run()
    {
        DB::table('groups')->delete();
        
		$data = [
				['name'=>'Group1'],
				['name'=>'Group2'],
				['name'=>'Group3'],
				['name'=>'Group4'],
		];
		
		foreach($data as $item) {
			Group::create($item);	
		}
		
    }
} 