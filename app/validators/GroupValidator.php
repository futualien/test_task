<?php
namespace App\Validators;

use App\Validators\Interfaces\IValidated;
use Validator;

class GroupValidator implements IValidated
{
    public static $validators = [
        'name' => ['required', 'max:50'],
    ];
    
    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator|mixed
     */
    public static function validated(array $data)
    {
        return Validator::make($data, self::$validators);
    }
    
}