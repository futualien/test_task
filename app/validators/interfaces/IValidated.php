<?php
namespace App\Validators\Interfaces;

use Illuminate\Validation\Validator;


interface IValidated
{
    /**
     * @param array $data
     * @return Validator
     */
    public static function validated(array $data);
}
