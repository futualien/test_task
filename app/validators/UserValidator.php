<?php
namespace App\Validators;

use App\Validators\Interfaces\IValidated;
use Validator;

class UserValidator implements IValidated
{
    public static $validators = [
		'email' => ['email'],
        'last_name' => ['required', 'max:50'],
		'first_name' => ['required', 'max:50'],
    ];
    
    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator|mixed
     */
    public static function validated(array $data)
    {
        return Validator::make($data, self::$validators);
    }
    
}