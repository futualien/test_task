<?php
namespace App\Models;

class User extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $hidden = array('pivot');
	protected $fillable = ['first_name', 'last_name', 'email'];
	
	public function groups () {
		return $this->belongsToMany('App\Models\Group', 'user_to_groups', 'user_id', 'group_id');
	}
	
	public function toArray() {
		
		return [
				'id' => $this->id,
				'email' => $this->email,
				'first_name' => $this->first_name,
				'last_name' => $this->last_name,
				'created_at' => strtotime($this->created_at),
				'updated_at' => strtotime($this->updated_at),
				'state' => $this->state ? 'active' : 'non active',
				'groups' => $this->groups()->get()->toArray(),
		];
		
	}
}
