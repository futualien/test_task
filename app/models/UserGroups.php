<?php
namespace App\Models;

class UserGroups extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_to_groups';
	
	public $timestamps = false;
	
	protected $fillable = ['user_id', 'group_id'];
}
