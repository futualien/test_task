<?php
namespace App\Models;

class Group extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'groups';
	protected $hidden = array('pivot');
	protected $fillable = ['name'];
	
	public $timestamps = false;

	public function users () {
		return $this->belongsToMany('App\Models\User', 'user_to_groups', 'group_id', 'user_id');
	}
		
	public function toArray() {
		
		return [
				'id' => $this->id,
				'name' => $this->name,
		];
		
	}
}
