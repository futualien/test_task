<?php

namespace App\Services;
use App\Models\User;
use App\Models\UserGroups;
use App\Models\Group;

class UserService {
	
	public function addUserInGroup(User $user, Group $group) {
		if ($this->isUserInGroup($user, $group)) {
			return true;
		}
		
		return UserGroups::create(['user_id'=>$user->id, 'group_id'=>$group->id]);
	}
	
	public function removeUserFromGroups(User $user) {
		$groups = $user->groups()->get();
		
		foreach($groups as $group) {
			$this->removeUserFromGroup($user, $group);
		}
	}
	
	public function removeUserFromGroup(User $user, Group $group) {
		$userGroup = UserGroups::where('user_id', $user->id)->where('group_id', $group->id)->first();
		if ($userGroup) {
			return $userGroup->delete();
		}
		
		return false;
	}
	
	public function clearGroup(Group $group) {
		return UserGroups::where('group_id', $group->id)->delete();
	}
	
	
	public function isUserInGroup(User $user, Group $group) {
		$userGroup = UserGroups::where('user_id', $user->id)->where('group_id', $group->id)->get();
		return $userGroup->count()>0;
	}
	
	public function addUserInGroups(User $user, array $groups) {
		if (count($groups)>0) {
			foreach($groups as $group) {
				$this->addUserToGroup($user, $group);
			}
			return true;
		}
		
		return false;
	}
}